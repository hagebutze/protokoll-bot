package main

import (
	"encoding/json"
	"fmt"
	"github.com/mattermost/mattermost-server/v5/model"
	"gopkg.in/gomail.v2"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"os/signal"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Config struct {
	Mattermost struct {
		Domain         string `json:"domain"`
		BotName     string `json:"bot_name"`
		Email       string `json:"email"`
		Password    string `json:"password"`
		Username    string `json:"username"`
		FirstName   string `json:"first_name"`
		LastName    string `json:"last_name"`
		TeamName    string `json:"team_name"`
		ChannelName string `json:"channel_name"`
		DebugChannelName string `json:"debug_channel_name"`
	} `json:"mattermost"`
	NextCloud struct {
		Url            string `json:"url"`
		Password       string `json:"password"`
		Username       string `json:"username"`
		ProtokollsRoot string `json:"protokolls_root"`
	} `json:"nextcloud"`
	EMail struct {
		Smtp      string `json:"smtp"`
		SmtpPort  int    `json:"smtp_port"`
		Password  string `json:"password"`
		Username  string `json:"username"`
		Recipient string `json:"recipient"`
	} `json:"email"`
}

var client *model.Client4
var webSocketClient *model.WebSocketClient

var botUser *model.User
var botTeam *model.Team
var protokolChannel *model.Channel
var debugChannel *model.Channel

var config Config

func loadConfig(fileName string) (Config, error) {
	// Load the config
	configFile, err := os.Open(fileName)
	defer configFile.Close()
	if err != nil {
		return Config{}, err
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config, nil
}

func main() {
	var err error
	config, err = loadConfig("./config.json")

	if err != nil {
		panic(err)
	}

	SetupGracefulShutdown()

	client = model.NewAPIv4Client("https://" + config.Mattermost.Domain)

	// Lets test to see if the mattermost server is up and running
	MakeSureServerIsRunning()

	// lets attempt to login to the Mattermost server as the bot user
	// This will set the token required for all future calls
	// You can get this token with client.AuthToken
	LoginAsTheBotUser()

	// If the bot user doesn't have the correct information lets update his profile
	UpdateTheBotUserIfNeeded()

	// Lets find our bot team
	FindBotTeam()

	// This is an important step.  Lets make sure we use the botTeam
	// for all future web service requests that require a team.
	//client.SetTeamId(botTeam.Id)

	// Lets create a bot channel for logging debug messages into
	err = CreateBotChannelsIfNeeded()
	if err != nil {
		panic(err)
	}
	SendMsg(debugChannel, "_"+config.Mattermost.BotName+" has **started** running_", nil, nil)

	// Lets start listening to some channels via the websocket!
	webSocketClient, appErr := model.NewWebSocketClient4("wss://" + config.Mattermost.Domain, client.AuthToken)
	if appErr != nil {
		println("We failed to connect to the web socket")
		PrintError(appErr)
	}

	webSocketClient.Listen()

	go func() {
		for {
			select {
			case resp := <-webSocketClient.EventChannel:
				HandleWebSocketResponse(resp)
			}
		}
	}()

	// You can block forever with
	select {}
}

func MakeSureServerIsRunning() {
	if props, resp := client.GetOldClientConfig(""); resp.Error != nil {
		println("There was a problem pinging the Mattermost server.  Are you sure it's running?")
		PrintError(resp.Error)
		os.Exit(1)
	} else {
		println("Server detected and is running version " + props["Version"])
	}
}

func LoginAsTheBotUser() {
	if user, resp := client.Login(config.Mattermost.Email, config.Mattermost.Password); resp.Error != nil {
		println("There was a problem logging into the Mattermost server.  Are you sure ran the setup steps from the README.md?")
		PrintError(resp.Error)
		os.Exit(1)
	} else {
		botUser = user
	}
}

func UpdateTheBotUserIfNeeded() {
	if botUser.FirstName != config.Mattermost.FirstName || botUser.LastName != config.Mattermost.LastName || botUser.Username != config.Mattermost.Username {
		botUser.FirstName = config.Mattermost.FirstName
		botUser.LastName = config.Mattermost.LastName
		botUser.Username = config.Mattermost.Username

		if user, resp := client.UpdateUser(botUser); resp.Error != nil {
			println("We failed to update the Sample Bot user")
			PrintError(resp.Error)
			os.Exit(1)
		} else {
			botUser = user
			println("Looks like this might be the first run so we've updated the bots account settings")
		}
	}
}

func FindBotTeam() {
	if team, resp := client.GetTeamByName(config.Mattermost.TeamName, ""); resp.Error != nil {
		println("We failed to get the initial load")
		println("or we do not appear to be a member of the team '" + config.Mattermost.TeamName + "'")
		PrintError(resp.Error)
		os.Exit(1)
	} else {
		botTeam = team
	}
}

func CreateChannelIfNeeded(name string, displayName string, purpose string) (*model.Channel, error) {
	if channel, resp := client.GetChannelByName(name, botTeam.Id, ""); resp.Error != nil {
		println("We failed to get the channels")
		PrintError(resp.Error)
	} else {
		return channel, nil
	}

	// Looks like we need to create the channel
	channel := &model.Channel{}
	channel.Name = name
	channel.DisplayName = displayName
	channel.Purpose = purpose
	channel.Type = model.CHANNEL_OPEN
	channel.TeamId = botTeam.Id
	if channel, resp := client.CreateChannel(channel); resp.Error != nil {
		println("We failed to create the channel " + channel.Name)
		PrintError(resp.Error)
		return nil, resp.Error
	} else {
		println("Looks like this might be the first run so we've created the channel " + config.Mattermost.ChannelName)
		return channel, nil
	}
}

func CreateBotChannelsIfNeeded() error {
	c, err := CreateChannelIfNeeded(config.Mattermost.ChannelName, "Protokolle vom Plenum", "Hier werden Protokolle vom Plenum verteilt")
	if err != nil {
		return err
	}
	protokolChannel = c
	c, err = CreateChannelIfNeeded(config.Mattermost.DebugChannelName, "Debug Channel des Protokoll Bots", "Hier sagt der Protokoll Bot was so geht")
	if err != nil {
		return err
	}
	debugChannel = c
	return nil
}

func SendMessageWithFile(channel *model.Channel, msg string, localFileName string, uplaodFileName string, replyTo *model.Post) error {
	println("Uploading file to channel")
	b, err := ioutil.ReadFile(localFileName)
	if err != nil {
		println("We failed to open the file to send to the logging channel")
		return err
	}
	fu, resp := client.UploadFile(b, channel.Id, uplaodFileName)
	if resp.Error != nil {
		println("We failed to send a file to the logging channel")
		PrintError(resp.Error)
		return err
	}
	fileId := fu.FileInfos[0].Id
	println("Uploaded, sending message")
	err = SendMsg(channel, msg, replyTo, []string{fileId})
	return err
}

func SendMsg(channel *model.Channel, msg string, replyTo *model.Post, fileIds model.StringArray) error {
	post := &model.Post{}
	post.ChannelId = channel.Id
	post.Message = msg
	if fileIds != nil {
		post.FileIds = fileIds
	}

	if replyTo != nil {
		post.RootId = replyTo.RootId
		if post.RootId == "" {
			post.RootId = replyTo.Id
		}
	}

	if _, resp := client.CreatePost(post); resp.Error != nil {
		println("We failed to send a message to the logging channel")
		PrintError(resp.Error)
		return resp.Error
	}
	return nil
}

func HandleWebSocketResponse(event *model.WebSocketEvent) {
	HandleMessage(event)
}

func FindDateInString(s string) *time.Time {
	res := []*regexp.Regexp{
		regexp.MustCompile(`(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.(\d{4}|\d{2})`),
		regexp.MustCompile(`(3[01]|[12][0-9]|0?[1-9])-(1[012]|0?[1-9])-(\d{4}|\d{2})`),
		regexp.MustCompile(`(3[01]|[12][0-9]|0?[1-9])(1[012]|0?[1-9])(\d{4}|\d{2})`),
	}

	for _, re := range res {
		match := re.FindStringSubmatch(s)
		if match == nil {
			continue
		}
		day, err := strconv.ParseInt(match[1], 10, 32)
		if err != nil {
			continue
		}
		month, err := strconv.ParseInt(match[2], 10, 32)
		if err != nil {
			continue
		}
		year, err := strconv.ParseInt(match[3], 10, 32)
		if year < 100 {
			year = year + 2000
		}
		if err != nil {
			continue
		}
		timeZone, _ := time.LoadLocation("Europe/Berlin")
		result := time.Date(int(year), time.Month(month), int(day), 0, 0, 0, 0, timeZone)
		return &result
	}
	// No match found
	return nil
}

func FindDateInPost(post *model.Post) *time.Time {
	res := FindDateInString(post.Message)
	if res != nil {
		return res
	}
	if post.FileIds != nil {
		for _, fileId := range post.FileIds {
			fileInfo, _ := client.GetFileInfo(fileId)
			res = FindDateInString(fileInfo.Name)
			if res != nil {
				return res
			}
		}
	}
	return nil
}

type MimeType int
const (
	UNKOWN MimeType = 0
	PDF MimeType = 1
	DOCX MimeType = 2
	ODT MimeType =3
)

func FileHandlableMimeType(fileId string) MimeType {
	fileInfo, _ := client.GetFileInfo(fileId)
	if fileInfo.MimeType == "application/pdf" {
		return PDF
	}
	if fileInfo.MimeType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" {
		return DOCX
	}
	if fileInfo.MimeType == "application/vnd.oasis.opendocument.text" {
		return ODT
	}
	println("Unkown mime typ: " + fileInfo.MimeType)
	return UNKOWN
}

func DownloadAndConvertFile(fileId string) error {
	// Download the file
	i, r := client.GetFileInfo(fileId)
	if r.Error != nil {
		return r.Error
	}
	b, r := client.GetFile(fileId)
	if r.Error != nil {
		return r.Error
	}
	fileName := "protokoll." + i.Extension
	err := ioutil.WriteFile(fileName, b, 0644)

	// Run unconv
	if i.MimeType != "application/pdf" {
		cmd := exec.Command("doc2pdf", fileName)
		err = cmd.Run()
		if err != nil {
			return err
		}
	}
	return nil
}

func SendProtokollMail(date time.Time) error {
	dateString := date.Format("02.01.2006")
	//dateFileString := date.Format("2006-01-02")
	//fileName := fmt.Sprintf("protokoll_%s.pdf", dateFileString)

	m := gomail.NewMessage()
	m.SetHeader("From", config.EMail.Username)
	m.SetHeader("To", config.EMail.Recipient)
	m.SetHeader("Subject", "Protokoll vom " + dateString)
	body := `
	Hallo Ihr Lieben Butzen,

Ein Protokoll wurde geschrieben! Und ich möchte es euch nicht vorenthalten. So schaut in den Anhang.

Euer treuer Protokoll Bot
	`
	m.SetBody("text/plain", body)
	m.Attach("./protokoll.pdf")

	d := gomail.NewDialer(config.EMail.Smtp, config.EMail.SmtpPort, config.EMail.Username, config.EMail.Password)

	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}

func UploadToNextCloud(date time.Time) error {
	year := date.Format("2006")
	monthNumber := date.Format("01")
	day := date.Format("02")
	monthName := ""
	switch monthNumber {
	case "01":
		monthName = "Januar"
	case "02":
		monthName = "Februar"
	case "03":
		monthName = "März"
	case "04":
		monthName = "April"
	case "05":
		monthName = "Mai"
	case "06":
		monthName = "Juni"
	case "07":
		monthName = "Juli"
	case "08":
		monthName = "August"
	case "09":
		monthName = "September"
	case "10":
		monthName = "Oktober"
	case "11":
		monthName = "November"
	case "12":
		monthName = "Dezember"
	}
	// Create folders
	yearPath := fmt.Sprintf("%s/remote.php/webdav%s/%s", config.NextCloud.Url, config.NextCloud.ProtokollsRoot, year)
	monthPath := fmt.Sprintf("%s/remote.php/webdav%s/%s/%s_%s", config.NextCloud.Url, config.NextCloud.ProtokollsRoot, year, monthNumber, monthName)
	usernamePassword := fmt.Sprintf("%s:%s", config.NextCloud.Username, config.NextCloud.Password)
	cmd := exec.Command("curl", "-u", usernamePassword,"-X","MKCOL","-k", yearPath)
	cmd.Run()
	cmd = exec.Command("curl", "-u", usernamePassword,"-X","MKCOL","-k", monthPath)
	cmd.Run()

	// Uplaod file
	uploadPath := fmt.Sprintf("%s/remote.php/dav/files/%s%s/%s/%s_%s/%s-%s-%s_Protokoll.pdf",
		config.NextCloud.Url,
		config.NextCloud.Username,
		config.NextCloud.ProtokollsRoot,
		year,
		monthNumber,
		monthName,
		year,
		monthNumber,
		day)
	println("Uplaod path: " + uploadPath)
	cmd = exec.Command("curl", "-f", "-u", usernamePassword, "-T", "protokoll.pdf", uploadPath)
	err := cmd.Run()
	if err != nil {
		return err
	}
	return nil
}

func HandleMessage(event *model.WebSocketEvent) {
	fmt.Println(event)

	// The channel over which this was send
	if event.GetBroadcast().ChannelId == "" {
		println("message with no channel, ignoring")
		return
	}
	channel, resp := client.GetChannel(event.GetBroadcast().ChannelId, "")
	if resp.Error != nil {
		println("unable to get the channel, over which this was send")
		return
	}
	// I actually care only about event send to me directly
	if channel.Type != "D" {
		println("Not a direct message, ignoring")
		return
	}

	// Lets only reponded to messaged posted events
	if event.EventType() != model.WEBSOCKET_EVENT_POSTED && event.EventType() != model.WEBSOCKET_EVENT_POST_EDITED {
		return
	}

	println("responding to personal channel msg")

	post := model.PostFromJson(strings.NewReader(event.GetData()["post"].(string)))


	if post != nil {

		// ignore my events
		if post.UserId == botUser.Id {
			return
		}

		// Get the full thread and sort it
		thread, response := client.GetPostThread(post.Id, "")
		if response.Error != nil {
			println("Error when getting thread: " + response.Error.Message)
		}
		thread.SortByCreateAt()
		for i := len(thread.Order)/2-1; i >= 0; i-- {
			opp := len(thread.Order)-1-i
			thread.Order[i], thread.Order[opp] = thread.Order[opp], thread.Order[i]
		}

		// Search for a message indicating I am already done
		for _, postId := range thread.Order {
			if thread.Posts[postId].UserId == botUser.Id {
				if thread.Posts[postId].Message == "Fertig!" || strings.HasPrefix(thread.Posts[postId].Message, "Danke für das Protokoll vom") {
					SendMsg(channel, "Ich habe hier schon alles erledigt, ich mach nichts mehr!", post, nil)
					return
				}
			}
		}

		// Search or dates
		var date *time.Time = nil
		for _, postId := range thread.Order {
			newDate := FindDateInPost(thread.Posts[postId])
			if newDate != nil {
				date = newDate
			}
		}

		// Search for files
		foundHandleableFile := false
		foundUnhandleableFile := false
		bestFileId := ""
		for _, postId := range thread.Order {
			post := thread.Posts[postId]
			if post.FileIds == nil {
				continue
			}
			for _, fileId := range post.FileIds {
				if mimeType := FileHandlableMimeType(fileId); mimeType != UNKOWN {
					foundHandleableFile = true
					bestFileId = fileId
				} else {
					foundUnhandleableFile = true
				}
			}
		}

		if !foundHandleableFile && !foundUnhandleableFile {
			switch rand.Intn(8) {
			case 0:
				SendMsg(channel, "Hey, toll das du mit mir redest. Ich verstehe aber nichts, ich will nur Protokolle!", post, nil)
				break
			case 1:
				SendMsg(channel, "Ja bitte? Ganz super, aber ohne Protokoll macht deine Nachricht für mich keinen Sinn.", post, nil)
				break
			case 2:
				SendMsg(channel, "Ok. Spannend. Ich versteh zwar nichts, denn ich verstehe nur Protokolle, aber trotzdem spannend!", post, nil)
				break
			case 3:
				SendMsg(channel, "Bla bla bla, gibt mir eine Protokoll Datei!", post, nil)
				break
			case 4:
				SendMsg(channel, "I brauche Protokolle! Protokolle Protokolle Protokolle! Keinen Smalltalk.", post, nil)
				break
			case 5:
				SendMsg(channel, "Ist das ein Protokoll? Nein? Dann verstehe ich es nicht.", post, nil)
				break
			case 6:
				SendMsg(channel, "Kein Protokoll, keine hilreiche Reaktion von mir.", post, nil)
				break
			case 7:
				SendMsg(channel, "Danke für das Protokoll, ich werde ... moment, da ist gar kein Protokoll. Ich kann gar nichts machen :(.", post, nil)
				break
			}
			return
		}

		if !foundHandleableFile {
			SendMsg(channel, "Ich kann mit dem Dateiformat nichts anfangen. Bitt nur pdf, odt oder docx Dateien!.", post, nil)
			return
		}

		if date == nil {
			SendMsg(channel, "OK, da ist eine Datei, und ich denke mal eine Protokoll. Aber von wann? Magst du mir das Datum nennen?!?.", post, nil)
			return
		}
		// Got all!
		SendMsg(channel, "Danke für das Protokoll vom " + date.Format("02.01.2006") + ". Ich werde versuchen es zu verbreiten.", post, nil)
		err := DownloadAndConvertFile(bestFileId)
		if err != nil {
			SendMsg(channel, "Leider konnte ich deine Datei nicht konvertieren. Der Fehler war: " + err.Error(), post, nil)
		} else {
			SendMsg(channel, "Ich habe die Datei runtergeladen und nach PDF konvertiert. Das hat schonmal geklappt.", post, nil)
		}
		// Upload to nextcloud
		err = UploadToNextCloud(*date)
		if err != nil {
			SendMsg(channel, "Leider konnte ich nicht nach NextCloud hochladen. Der Fehler war: " + err.Error(), post, nil)
		} else {
			SendMsg(channel, "Nextcloud hat die Datei aufgenommen. Ich bin also fast fertig.", post, nil)
		}
		// Send mail
		err = SendProtokollMail(*date)
		if err != nil {
			SendMsg(channel, "Leider konnte ich die Mail nicht senden. Der Fehler war: " + err.Error(), post, nil)
		} else {
			SendMsg(channel, "Auch die Mail habe ich erfolgreich versendet.", post, nil)
		}
		err = SendMessageWithFile(protokolChannel,fmt.Sprintf("Neues Protokoll vom %s, vielen dank %s", date.Format("02.01.2006"), event.GetData()["sender_name"]), "./protokoll.pdf", "protokoll.pdf", nil)
		if err != nil {
			SendMsg(channel, "Mmmh, das hochladen im Mattermost Protokoll channel hat nicht funktioniert, der Fehler war: " + err.Error(), post, nil)
		} else {
			SendMsg(channel, "So, im Protokoll-Channel ist das Protokoll jetzt auch zu finden.", post, nil)
		}
		SendMsg(channel, "Fertig!", post, nil)
	}
}

func PrintError(err *model.AppError) {
	println("\tError Details:")
	println("\t\t" + err.Message)
	println("\t\t" + err.Id)
	println("\t\t" + err.DetailedError)
}

func SetupGracefulShutdown() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for _ = range c {
			if webSocketClient != nil {
				webSocketClient.Close()
			}

			SendMsg(debugChannel, "_"+config.Mattermost.BotName+" has **stopped** running_", nil, nil)
			os.Exit(0)
		}
	}()
}
