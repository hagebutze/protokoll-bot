module protokoll-bot

go 1.12

require (
	github.com/mattermost/mattermost-server/v5 v5.20.1
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/remogatto/cloud v0.0.0-20191009090237-dcf2638e6bc7
	github.com/remogatto/prettytest v0.0.0-20200211072524-6d385e11dcb8 // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
